# Ambient Orbs

This is for the David Rose display in Wicked Smart, as part of the Innovation Hub and in partnership with WGBH. He created a product called “Ambient Orbs” that would display simple forms of data such as the current temperature or stock prices with an orb that changes color to match the data. The product was discontinued some time ago, and the services were all shut down.

David suggested using the Phillips Hue system, and Jimmie Rodgers said that it would be doable to recreate some of those features with it. After reviewing, folks weren’t happy with weather being the only reasonable data source easily available, and Ben Wilson pointed out that we have a number of internal data feeds to use. Ultimately the tickets to the Butterfly Garden was chosen, as ticket data is easily accessed from inside the building at: https://triton.mos.org/products/productsTodayByVenueShow.xml

The setup uses a Philips Hue Bridge (used to control the lights wirelessly), two Philips Hue candelabra bulbs (only ones that would fit inside the orbs), and a Raspberry Pi. The Raspberry Pi is configured to start a python script, which then loops indefinitely to set the bulb colors and blinking patterns. It requires a connection to the data feed and an internet connection for the Hub to communicate to Phillips. It’s unclear if the internet connection is needed for sure, but our ethernet jacks provide it, so we may as well let it.

Keith Simmons wrote the XML parser that the runs on, and also set up the automation portion. On Raspberry Pi startup /etc/rc.local runs, which then runs startup_hue.sh, that then runs /home/pi/butterfly_production.py which update everything as long as the Pi is powered. The script can handle running overnight and re-starting it’s counters, but the bulbs would maintain a 0 ticket sales during the time that tickets cannot be sold.


# Issues

1. ~~It's possible the IP addresses will change for both the Phillips Hub, and for the Raspberry Pi. This will break both of the device's ability to communicate with each other. Jimmie will contact Eric in IIT to solve this before it goes live.~~ **Closed** Eric has changed the network to give both static IPs.

2. ~~If internet is lost, the python script will exit with an error, as there is no error handler in the program. It's unclear how often this is likely to occur, but a reboot will fix it. Since the lights will remain the last color given, there is no current way to verify that it is running unless a change happens.~~ **Closed** Jimmie added event handling into the program, so now it will simply hold state and wait 5 minutes before trying to access the network again. Once the network comes back it will resume, which has come up surprisingly often.

3. ~~It would be very handy to have the Raspberry Pi email whenever it encounters a problem, either if it loses connection per problem 2, or has a different IP as per problem 1. Sending an email on boot of the IP and then sending emails with any errors would be very helpful long term.~~ **Closed** Jimmie added email for the script that sends IP, light status, and time. It is currently configured to go to jrodgers@mos.org, using mosexhibit@gmail.com

4. ~~There does not appear to be any source of NTP avaliable on the network port, Eric has been contacted about it~~ **Closed** Jimmie ordered a [hardware RTC for the Pi](https://www.adafruit.com/product/3386) and installed it to try for NTP first but use the battery backuped RTC if that fails on boot. Shortly afterward Eric opend up the network port for NTP as well, and everything seems to be working as intended. While NTP is now functional, the RTC will remain on the Pi to make sure.