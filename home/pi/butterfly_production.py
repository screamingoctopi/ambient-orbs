#!/usr/bin/python
# -*- coding: utf-8 -*-
# by Keith Simmons and Jimmie P. Rodgers
# etree doesn't support full XPATH, so use lxml
from lxml import etree as ET
import urllib2
import xmltodict
from phue import Bridge
import random
import sys
from butterfly_config import *
from time import *
from datetime import *
import subprocess
import smtplib
import socket
from email.mime.text import MIMEText

try:
    # Initializing the Hue Bridge and setting up lights
    b = Bridge(HUE_IP)  # Enter bridge IP here.
    # If running for the first time, press button on bridge and run with b.connect() uncommented
    # b.connect()
    lights = b.get_light_objects()
    bulb_brightness = 254
    command = {'transitiontime': 5,'on': False,'bri': bulb_brightness,'colormode': 'xy',}
    b.set_light([0, 1, 2, 3], command)
    show = 0    # Address of the current show sales light
    sales = 1   # Address of the total sales light
    # Turns on both bulbs to have a color by default
    lights[sales].on = True
    lights[sales].brightness = bulb_brightness
    lights[sales].xy = [0, 0.5] # cyan
    lights[show].on = True
    lights[show].brightness = bulb_brightness
    lights[show].xy = [0.4, 0]  # magenta
except:
    light_status = 'NOT on'
else:
    light_status = 'on'

# These are the time variables now_time is now, next_time is in 15 minutes
# The first time next_time is set behind now so that it runs
next_time = datetime.time(datetime.now() + timedelta(seconds=-1))
known_date = datetime.date(datetime.now())
now_time = datetime.time(datetime.now())

# Sends an email to indicate that the pi is on and that the lights are working
to = 'jrodgers@mos.org'
gmail_user = 'mosexhibit@gmail.com'
gmail_password = 'MOSt3mpP4ss!'
smtpserver = smtplib.SMTP('smtp.gmail.com', 587)
smtpserver.ehlo()
smtpserver.starttls()
smtpserver.ehlo
smtpserver.login(gmail_user, gmail_password)
today = datetime.date(datetime.now())
# Very Linux Specific
arg='ip route list'
p=subprocess.Popen(arg,shell=True,stdout=subprocess.PIPE)
data = p.communicate()
split_data = data[0].split()
ipaddr = split_data[split_data.index('src')+1]
my_ip = 'Your ip is %s.\nThe lights are %s\nThe time is currently: %s' %  (ipaddr, light_status, now_time)
msg = MIMEText(my_ip)
msg['Subject'] = 'IP For RaspberryPi on %s' % today.strftime('%b %d %Y')
msg['From'] = gmail_user
msg['To'] = to
smtpserver.sendmail(gmail_user, [to], msg.as_string())
smtpserver.quit()

total_tickets_sold = 0      # Total tickets sold for all shows
last_total_tickets_sold = 0 # Previous value stored for comparison
tickets_to_display = 0      # Times the ticket sales led has to blink
max_tickets = 0             # Total possible tickets for all shows
sleep_time = 2.0            # Seconds the ticket sales led blinks off then on for
delay_seconds = 30          # Time between updates
max_blink = 10              # Max times the ticket sales led blinks at a time
update = False              # Toggled whenever total_ticket_sold increases
first_loop = True           # Used to control the first loop in the main update
verbose = False             # Outputs verbose information
debug = True                # Outputs variable information
startup_blink = False       # Shows blinks for already sold tickets on startup
very_first_run = True       # Used for the very first time running the loop
if debug:
    print 'Starting infinite loop'

while True:
    # Gets the current time and date
    now_time = datetime.time(datetime.now())
    current_date = datetime.date(datetime.now())
    # Resets the important variables if left on overnight
    if current_date > known_date:
        last_total_tickets_sold = 0 # Most important to reset on new date
        known_date = current_date   # Used to check for new day
        very_first_run = True
        update = False
        first_loop = True

    if now_time > next_time:
        try:
            # This grabs the latest XML data
            file = urllib2.urlopen('https://triton.mos.org/products/productsTodayByVenueShow.xml')
            data = file.read()
            file.close()
            root = ET.fromstring(data)
            # Keith's beautiful syntax for getting venue node w/ butterfly title
            butterfly_venue_element = root.find("venue[title='Butterfly Garden']")
            butterfly_capacity = int(butterfly_venue_element.find('capacity').text)
        except:
            print "There is an error with the XML!"
            update = False
            sleep(300)
        else:
            if debug:
                print 'Now > Next'
                print 'Now_time: ', now_time
            # Clears ticket counts for this round
            total_tickets_sold = 0
            max_tickets = 0

            # iterate through all show times
            for element in butterfly_venue_element.findall('shows/show/date/times/time'):
                # This runs only once, to set the next time it will run to be
                # delay_seconds from the current time
                if first_loop:
                    update = True
                    first_loop = False
                    next_time = datetime.time(datetime.now()+ timedelta(seconds=delay_seconds))
                    if debug:
                        print 'next_time: ', next_time
                        print ' '

                # Jimmie's beautiful syntax for getting the right time types for easy
                # comparisons later in the code.
                start_time = datetime.time(datetime.strptime(element.find('starttime').text, '%H:%M'))
                future_time = datetime.time(datetime.now()+ timedelta(minutes=15))
                tickets_in_stock = int(element.find('instock').text)
                tickets_sold = butterfly_capacity - tickets_in_stock
                total_tickets_sold += tickets_sold
                max_tickets += butterfly_capacity
                if verbose:
                    print 'Start time: %s ' % start_time
                    print 'In stock: %s ' % tickets_in_stock
                    print 'Sold: %s ' % tickets_sold
                    if datetime.time(datetime.now()) < start_time:
                        print 'Show has not started yet'
                    else:
                        print 'Show has started'
                    print " "

                # Starts if the time is between 0 an 15 minutes from the start of a show.
                # This sets the show bulb to the ticket_sold range and color.
                if now_time <= start_time and future_time >= start_time:
                    lights[show].on = True
                    lights[show].brightness = bulb_brightness
                    if debug:
                        print "******************************************"
                        print 'Setting light 0'
                        print 'Show time: %s ' % start_time
                        print 'Current time: ', now_time
                        print 'Tickets_sold: ', tickets_sold
                    if tickets_sold >= 0 and tickets_sold <= 4:
                        lights[show].xy = [0, 0]  # blue
                        if debug:
                            print 'Color: Blue 0-6'
                    elif tickets_sold >= 5 and tickets_sold <= 9:
                        lights[show].xy = [0.4, 0]  # magenta
                        if debug:
                            print 'Color: Magenta 7-12'
                    elif tickets_sold >= 10 and tickets_sold <= 15:
                        lights[show].xy = [1, 0]  # red
                        if debug:
                            print 'Color: Red 13-18'
                    elif tickets_sold >= 16 and tickets_sold <= 22:
                        lights[show].xy = [0.5, 0.5]  # yellow
                        if debug:
                            print 'Color: Yellow 19-24'
                    elif tickets_sold >= 23 and tickets_sold <= 30:
                        lights[show].xy = [0, 1]  # green
                        if debug:
                            print 'Color: Green 25-30'
                    if debug:
                        if debug:
                            print "******************************************"
                        if debug or verbose:
                            print ' '
            if debug or verbose:
                print 'Total tickets sold: %s/%s' % (total_tickets_sold,max_tickets)
                print " "

    # This is the main update loop for the ticket sales bulb.
    # If there is an update or there are tickets_to_display it will enter this.
    if update or tickets_to_display > 0:
        update = False
        first_loop = True
        if very_first_run:
            very_first_run = False
            if not startup_blink:
                last_total_tickets_sold = total_tickets_sold
        if total_tickets_sold > last_total_tickets_sold:
            if debug:
                print 'Setting light 1'
                print 'Total tickets sold: %s/%s' % (total_tickets_sold, max_tickets)
                print 'Last tickets: ', last_total_tickets_sold
            tickets_to_display += total_tickets_sold - last_total_tickets_sold
            last_total_tickets_sold = total_tickets_sold
        elif last_total_tickets_sold > total_tickets_sold:
            last_total_tickets_sold = total_tickets_sold

        if tickets_to_display >0:
            if tickets_to_display > max_blink:
                loops = random.randint(0, max_blink)
            else:
                loops = tickets_to_display
            for x in range(0, loops):
                if debug:
                    print 'Tickets to display: %s %s/%s' % (tickets_to_display,loops-x,loops)
                lights[sales].on = False
                sleep(sleep_time)
                lights[sales].on = True
                lights[sales].brightness = bulb_brightness
                lights[sales].xy = [0, 0.5]  # cyan
                sleep(sleep_time)
                tickets_to_display -= 1
            if debug:
                print 'Tickets left to display: ', tickets_to_display


#EOF
