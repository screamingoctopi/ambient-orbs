# by Keith Simmons and Jimmie P. Rodgers

# etree doesn't support full XPATH, so use lxml
from lxml import etree as ET
import urllib2
import time

file = urllib2.urlopen('https://triton.mos.org/products/productsTodayByVenueShow.xml')
data = file.read()
file.close()

root = ET.fromstring(data)

# beautiful syntax for getting venue node w/ butterfly title
butterfly_venue_element = root.find("venue[title='Butterfly Garden']")

# total number of tickets available for any show (all times)
butterfly_capacity = int(butterfly_venue_element.find("capacity").text)

total_tickets_sold = 0
max_tickets = 0

# iterate through all show times
for element in butterfly_venue_element.findall("shows/show/date/times/time"):
        start_time = element.find("starttime").text
        tickets_in_stock = int(element.find("instock").text)
        tickets_sold = butterfly_capacity - tickets_in_stock
        total_tickets_sold += tickets_sold
        max_tickets += butterfly_capacity

        print "start time: %s " % start_time
        print "in stock: %s " % tickets_in_stock
        print "sold: %s " % tickets_sold

        # if current time is before show starts
        # NOTE: this method only works if all times are 2 digit 0-padded numbers
        #       otherwise please compare in datetime format
        if time.strftime('%H:%M') < start_time:
            print "show has not started yet"
        else:
            print "show has started"

        print "\n"

print "Total tickets sold: %s/%s" %(total_tickets_sold,max_tickets)
