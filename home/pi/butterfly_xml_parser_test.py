# by Keith Simmons and Jimmie P. Rodgers
# etree doesn't support full XPATH, so use lxml
from lxml import etree as ET
import urllib2
from time import *
from datetime import *

while True:
    try:
        file = urllib2.urlopen('https://triton.mos.org/products/productsTodayByVenueShow.xml')
        data = file.read()
        file.close()
    except:
        print "\n\nXML Failure\n\n"
        sleep(30)
    else:
        root = ET.fromstring(data)
        # beautiful syntax for getting venue node w/ butterfly title
        butterfly_venue_element = root.find("venue[title='Butterfly Garden']")
        # total number of tickets available for any show (all times)
        butterfly_capacity = int(butterfly_venue_element.find("capacity").text)
        # iterate through all show times
        total_tickets_sold = 0
        max_tickets = 0
        now_time = datetime.time(datetime.now())
        for x in range(0, 7):
            print "\n"
        for element in butterfly_venue_element.findall("shows/show/date/times/time"):
            start_time = datetime.time(datetime.strptime(element.find('starttime').text, '%H:%M'))
            future_time = datetime.time(datetime.now()+ timedelta(minutes=15))
            tickets_in_stock = int(element.find("instock").text)
            tickets_sold = butterfly_capacity - tickets_in_stock
            total_tickets_sold += tickets_sold
            max_tickets += butterfly_capacity
            if now_time <= start_time and future_time >= start_time:
                current =  " <-\n"
                print ""
            else:
                current = ""
            print " Start Time: %s   Sold: %s%s" % (start_time, tickets_sold, current)

            # if current time is before show starts
            # NOTE: this method only works if all times are 2 digit 0-padded numbers
            #       otherwise please compare in datetime format
            """
            if datetime.time(datetime.now()) < start_time:
                print "show has not started yet"
            else:
                print "show has started"
            """
            #print " "
        print "-------------------------------"
        print " Total tickets sold:   %s/%s" %(total_tickets_sold,max_tickets)
        print "*******************************"
        print ""
        sleep(30)
